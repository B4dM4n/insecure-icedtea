{ pkgs ? import <nixpkgs> { } }:

with pkgs;
let
  insecure-jdk = lib.fix (self:
    jdk8.overrideAttrs (o: {
      postFixup = (o.postFixup or "") + ''
        # Remove all occurences off `jdk.tls.disabledAlgorithms` on a single line (expression 1)
        # and multiple lines (expression 2)
        sed -i \
          -e '/^jdk.tls.disabledAlgorithms.*[^\\]$/d' \
          -e '/^jdk.tls.disabledAlgorithms.*\\$/,/[^\\]$/d' \
          $out/lib/openjdk/jre/lib/security/java.security \
          $jre/lib/openjdk/jre/lib/security/java.security
      '';
      passthru = o.passthru // { home = "${self}/lib/openjdk"; };
    }));
in
adoptopenjdk-icedtea-web.override { jdk = insecure-jdk; }
