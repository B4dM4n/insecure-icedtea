{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
  };
  outputs = { self, nixpkgs }:
    with nixpkgs.lib;
    let
      forAllSystems = genAttrs [ "aarch64-linux" "x86_64-linux" ];
    in
    {
      packages = forAllSystems (system: {
        insecure-icedtea = nixpkgs.legacyPackages.${system}.callPackage ./. { };
      });

      defaultPackage = forAllSystems (system: self.packages.${system}.insecure-icedtea);

      apps = forAllSystems (system:
        genAttrs [ "itweb-settings" "javaws" "policyeditor" ]
          (name: {
            type = "app";
            program = "${self.defaultPackage.${system}}/bin/${name}";
          }));

      defaultApp = forAllSystems (system: self.apps.${system}.javaws);

      checks = forAllSystems (system:
        let
          pkgs = nixpkgs.legacyPackages.${system};
          checks =
            mapAttrs'
              (name: value: {
                name = "apps-${name}";
                value = pkgs.runCommand "apps-${name}-test"
                  { } ''
                  fail() { echo "$@"; exit 1; }
                  test -x ${value.program} || fail "${value.program} is not executable"
                  touch $out
                '';
              })
              self.apps.${system} //
            mapAttrs'
              (name: value: {
                name = "packages-${name}";
                inherit value;
              })
              self.packages.${system};
        in
        checks // {
          all = pkgs.linkFarm "checks" (mapAttrsToList (name: path: { inherit name path; }) checks);
        });
    };
}
